{
  inputs = {
    systems.url = "github:nix-systems/default";
  };

  outputs = {
    systems,
    nixpkgs,
    self,
    ...
  } @ inputs: let
    forAllSystems = nixpkgs.lib.genAttrs (import systems);
    meta = {
      name = "testcafe";
      version = "3.6.1";
    };
  in {
    packages = forAllSystems (system: let
      pkgs = import nixpkgs {inherit system;};
    in rec {
      default = testcafe;
      testcafe = pkgs.writeShellApplication {
        name = "${meta.name}";
        runtimeInputs = with pkgs; [nodejs_22];
        text = ''
          npx --yes ${meta.name}@${meta.version} "$@"
        '';
      };
    });
  };
}
